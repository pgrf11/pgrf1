package fill;

import model.Edge;
import model.Point;
import model.Polygon;
import rasterize.PolygonRasterizer;
import rasterize.Raster;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanLineFiller implements Filler {
    private final PolygonRasterizer polygonRasterizer;
    private final Polygon polygon;
    private final Raster raster;


    public ScanLineFiller(Raster raster, PolygonRasterizer polygonRasterizer, Polygon polygon) {
        this.raster = raster;
        this.polygonRasterizer = polygonRasterizer;
        this.polygon = polygon;
    }

    @Override
    public void fill() {
        scanLine();
    }

    private void scanLine() {
        if(polygon.getCount() < 2) return;
        List<Edge> edges = new ArrayList<>();

        for (int i = 0; i < polygon.getCount(); i++) {
            int nextIndex = (i + 1) % polygon.getCount();
            Point p1 = polygon.getPoint(i);
            Point p2 = polygon.getPoint(nextIndex);
            Edge edge = new Edge(p1.getX(), p2.getX(), p1.getY(), p2.getY());
            if (edge.isHorizontal())
                continue;
            edge.orientate();
            edge.calculate();
            edges.add(edge);

        }

        int yMin = polygon.getPoint(0).getY();
        int yMax = yMin;
        for (Point p : polygon.getPoints()) {
            if (p.getY() < yMin) {
                yMin = p.getY();
            }
            if (p.getY() > yMax) {
                yMax = p.getY();
            }

        }

        List<Integer> intersection = new ArrayList<>();
        for (int y = yMin; y <= yMax; y++) {
            intersection.clear();
            for (Edge e : edges) {
                if (e.isIntersection(y)) {
                    intersection.add((int) e.getIntersection(y));
                }
            }
            Collections.sort(intersection);
            for (int i = 0; i < intersection.size(); i += 2) {
                for (int j=intersection.get(i); j<=intersection.get(i+1);j++) {
                    raster.setPixel(j, y, Color.green.getRGB());
                }
            }
        }
        polygonRasterizer.rasterize(polygon);
    }
}

