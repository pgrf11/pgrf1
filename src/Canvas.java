import cutter.EdgeCutter;
import fill.Filler;
import fill.ScanLineFiller;
import fill.SeedFiller;
import model.*;
import model.Point;
import model.Polygon;
import rasterize.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;


public class Canvas {
    private final JFrame frame;
    private final JPanel panel;
    private RasterBufferImage raster;
    private LineRasterizer lineRasterizer;
    private Polygon polygon1;
    private Polygon polygon2;
    private PolygonRasterizer polygonRasterizer;
    private ArrayList <Line> lines = new ArrayList<>();
    private SeedFiller seedFiller;
    private Edge edge;
    private EdgeCutter edgeCutter;



    public Canvas(int width, int height)
    {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("PGRF1");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferImage(800, 600);
        lineRasterizer = new LineRasterizerTrivial(raster);
        polygonRasterizer = new PolygonRasterizer(lineRasterizer);
        edgeCutter = new EdgeCutter();
        polygon1= new Polygon();
        polygon2= new Polygon();
        polygon1.addPoint(new Point(200,200));
        polygon1.addPoint(new Point(400,200));
        polygon1.addPoint(new Point(400,400));
        polygon1.addPoint(new Point(200,400));

        polygon2.addPoint(new Point(300,300));
        polygon2.addPoint(new Point(300,500));
        polygon2.addPoint(new Point(500,500));
        polygon2.addPoint(new Point(500,300));

        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                raster.present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();

        panel.addMouseMotionListener(new MouseMotionAdapter(){

        });
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode()==KeyEvent.VK_C){
                    lines.clear();
                    polygon1.clear();
                    polygon2.clear();
                }
                animate();
            }
        });
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Interaction.clientX = e.getX();
                Interaction.clientY = e.getY();
                Interaction.dragging = true;

                if (e.getButton() == MouseEvent.BUTTON1) {
                    polygon1.addPoint(new Point(Interaction.clientX, Interaction.clientY));
                }
                if (SwingUtilities.isMiddleMouseButton(e)){
                    polygon2.addPoint(new Point(Interaction.clientX, Interaction.clientY));
                    if (!polygon2.isConvex()){
                        polygon2.getPoints().remove(polygon2.getPoints().size()-1);
                    }
                }

                panel.repaint();
                animate();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                animate();
                if (e.getButton() == MouseEvent.BUTTON3) {
                    Filler seedFiller = new SeedFiller(raster, e.getX(), e.getY(),
                            0x00ff00,
                            Color.black.getRGB());
                    seedFiller.fill();
                }

            }


        });


    }

    public void animate(){
        raster.clear();
        for (Line line : lines) {
            lineRasterizer.rasterize(line);
        }
        polygonRasterizer.rasterize(polygon1);
        polygonRasterizer.rasterize(polygon2);

        Polygon newPoly = edgeCutter.clip(polygon1, polygon2);
        ScanLineFiller scanLineFiller = new ScanLineFiller(raster, polygonRasterizer, newPoly);
        scanLineFiller.fill();

        panel.repaint();
    }
}
