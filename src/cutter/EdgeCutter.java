package cutter;

import model.CutEdge;
import model.Point;
import model.Polygon;

import java.util.ArrayList;
import java.util.List;

public class EdgeCutter {
    public Polygon clip(Polygon polygon1, Polygon polygon2) {
        List <Point> points= new ArrayList<>();
        for(Point p: polygon1.getPoints()) points.add(p);
        List <Point> clipPoints= polygon2.getPoints();

        if (clipPoints.size() < 2) {
            return polygon1;
        }
        List<Point> newPoints = points;
        Point p1 = clipPoints.get(clipPoints.size() - 1);
        for (final Point p2 : clipPoints) {
            newPoints = (points = this.clipEdge(points, new CutEdge(p1, p2)));
            p1 = p2;
        }

        Polygon newPolygon = new Polygon();
        newPolygon.getPoints().addAll(newPoints);
        return newPolygon;
    }

    private List<Point> clipEdge(final List<Point> points, final CutEdge e) {
        if (points.size() < 2) {
            return points;
        }
        final List<Point> out = new ArrayList<Point>();
        out.clear();
        Point v1 = points.get(points.size() - 1);
        for (final Point v2 : points) {
            if (e.inside(v2)) {
                if (!e.inside(v1)) {
                    out.add(e.intersection(v1, v2));
                }
                out.add(v2);
            }
            else if (e.inside(v1)) {
                out.add(e.intersection(v1, v2));
            }
            v1 = v2;
        }
        return out;
    }
}

