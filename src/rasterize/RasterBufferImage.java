package rasterize;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.awt.Color.BLACK;

public class RasterBufferImage implements Raster {

    private BufferedImage img;

    public RasterBufferImage(int width, int height) {
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    }

    @Override
    public void setPixel(int x, int y, int color) {
        if (x<0 || y<0 || x>img.getWidth()-1 || y> img.getHeight()-1)
            return;

        img.setRGB(x, y, color);
    }

    @Override
    public int getPixel(int x, int y) {
        return img.getRGB(x, y);
    }

    public void present(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

    public void clear() {
        Graphics gr = img.getGraphics();
        gr.setColor(BLACK);
        gr.fillRect(0, 0, img.getWidth(), img.getHeight());
    }

    public BufferedImage getImg() {
        return img;
    }
}
