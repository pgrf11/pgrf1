package rasterize;

import model.Line;
import model.Point;
import model.Polygon;
import model.Interaction;

public class PolygonRasterizer {
    private LineRasterizer lineRasterizer;


    public PolygonRasterizer(LineRasterizer lineRasterizer) {
        this.lineRasterizer = lineRasterizer;
    }

    public void rasterize(Polygon polygon) {
        if (polygon.getCount()<=1)
            return;

        Point point1;
        Point point2;
        Point point3;
        Point point4;


        for (int i=0; i< polygon.getCount()-2;i++){
            point1= polygon.getPoint(i);
            point2= polygon.getPoint(i+1);
            lineRasterizer.rasterize(new Line(point1.getX(), point1.getY(), point2.getX(), point2.getY()));
        }
        point1= polygon.getPoint(polygon.getCount()-1);
        point2= polygon.getPoint(0);
        point3= polygon.getPoint(polygon.getCount()-2);
        point4= polygon.getPoint(polygon.getCount()-1);
        lineRasterizer.rasterize(new Line(point1.getX(), point1.getY(), point2.getX(), point2.getY()));
        lineRasterizer.rasterize(new Line(point3.getX(), point3.getY(), point4.getX(), point4.getY()));
    }

}
