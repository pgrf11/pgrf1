package rasterize;

public class LineRasterizerTrivial extends LineRasterizer {

    public LineRasterizerTrivial(Raster raster) {
        super(raster);
    }

    @Override
    protected void drawLine(int x1, int y1, int x2, int y2) {
        float k = (float)(y2 - y1) / (x2 - x1);
        float q = y1 - k * x1;

        if (Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
            int min = Math.min(x1, x2);
            int max = Math.max(x1, x2);

            for(int x = min, y; x <= max; x++) {
                y = (int)(k * x + q);
                raster.setPixel(x, y, 0xff0000);
            }
        } else {
            int min = Math.min(y1, y2);
            int max = Math.max(y1, y2);

            if(k == Double.POSITIVE_INFINITY || k == Double.NEGATIVE_INFINITY)
                for(int y = min; y <= max; y++) raster.setPixel(x1, y, 0xff0000);
            else {
                for(int y = min, x; y <= max; y++) {
                    x = (int)( (y - q) / k );
                    raster.setPixel(x, y, 0xff0000);
                }
            }

        }
    }

    }



