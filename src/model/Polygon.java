package model;

import fill.SeedFiller;

import java.util.ArrayList;
import java.util.List;

public class Polygon {

    protected List<Point> points;

    public Polygon() {
        this.points = new ArrayList<>();
    }

    public void addPoint(Point p) {
        points.add(p);
    }

    public Point getPoint(int index) {
        return points.get(index);
    }

    public int getCount() {
        return points.size();
    }

    public List<Point> getPoints() {
        return points;
    }

    public boolean isConvex()
    {
        if (points.size() < 4)
            return true;

        boolean sign = false;
        int n = points.size();

        for(int i = 0; i < n; i++)
        {
            double dx1 = points.get((i + 2) % n).getX() - points.get((i + 1) % n).getX();
            double dy1 = points.get((i + 2) % n).getY() - points.get((i + 1) % n).getY();
            double dx2 = points.get(i).getX() - points.get((i + 1) % n).getX();
            double dy2 = points.get(i).getY() - points.get((i + 1) % n).getY();
            double zcrossproduct = dx1 * dy2 - dy1 * dx2;

            if (i == 0)
                sign = zcrossproduct > 0;
            else if (sign != (zcrossproduct > 0))
                return false;
        }

        return true;
    }
    public void clear() {
        points.clear();
    }
}
