package model;

public class Edge {
    private  int x1, x2,x3, y1, y2, y3;
    public double k;
    public double q;

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getX3() {
        return x3;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }

    public int getY3() {
        return y3;
    }

    public Edge(int x1, int x2, int y1, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }


    public boolean isHorizontal(){
        return y1== y2;
    }
    public void orientate(){
        if (y2>y1){
            x3=x1;
            x1=x2;
            x2=x3;

            y3=y1;
            y1=y2;
            y2=y3;
            y2=y2;
        }
    }
    public boolean isIntersection(final int y) {
        return (y > y2 && y <= y1);
    }

    public double getIntersection(final int y) {
        float k = (float)(x2 - x1)/(y2 - y1);
        float q= x1 - k * y1;
        int x = (int)( k * y + q);
        return x;
    }
   public void calculate() {
       if (this.y1 != this.y2) {
           this.k = (this.x2 - this.x1) / (this.y2 - this.y1);
           this.q = this.x1 - this.k * this.y1;
       }
   }

}
