package model;

public class CutEdge {
    Point p1;
    Point p2;

    public CutEdge(final Point p1, final Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public boolean inside(final Point p) {
        final Point v1 = new Point(this.p2.getX() - this.p1.getX(), this.p2.getY() - this.p1.getY());
        final Point n1 = new Point(-v1.getY(), v1.getX());
        final Point v2 = new Point(p.getX() - this.p1.getX(), p.getY() - this.p1.getY());

        return n1.getX() * v2.getX() + n1.getY()* v2.getY() < 0;
    }

    public Point intersection(final Point v1, final Point v2) {
        final double px = ((v1.getX() * v2.getY() - v1.getY() * v2.getX()) * (this.p1.getX() - this.p2.getX()) - (this.p1.getX() * this.p2.getY() - this.p1.getY() * this.p2.getX()) * (v1.getX() - v2.getX())) / ((v1.getX() - v2.getX()) * (this.p1.getY() - this.p2.getY()) - (this.p1.getX() - this.p2.getX()) * (v1.getY() - v2.getY()));
        final double py = ((v1.getX() * v2.getY()- v1.getY() * v2.getX()) * (this.p1.getY() - this.p2.getY()) - (this.p1.getX() * this.p2.getY() - this.p1.getY() * this.p2.getX()) * (v1.getY() - v2.getY())) / ((v1.getX() - v2.getX()) * (this.p1.getY()- this.p2.getY()) - (this.p1.getX() - this.p2.getX()) * (v1.getY() - v2.getY()));
        return new Point((int)px, (int)py);
    }
}
